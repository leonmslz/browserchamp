function getCurrentTime() {
    let now = new Date();
    let time = 
    now.getHours().toLocaleString("en-US", {
        minimumIntegerDigits: 2,
        useGrouping: false
    }) 
    + ":" + 
    now.getMinutes().toLocaleString("en-US", {
        minimumIntegerDigits: 2,
        useGrouping: false
    });

    document.getElementById("currentTime").innerHTML = "<p>⏰" + time + "</p>";

    setTimeout(getCurrentTime, 1000);
}
getCurrentTime();

let bookmarks = [
    [116, "https://www.twitch.tv/directory/following"],
    [121, "https://www.youtube.com/feed/subscriptions"],
    [103, "https://www.gitlab.com"],
    [104, "https://www.github.com"],
    [100, "https://www.duckduckgo.com"],
    [117, "https://www.reddit.com/r/unixporn"]
];

document.addEventListener("keypress", (e) => {
    bookmarks.forEach((element, index) => {
        if (e.keyCode === element[0]) {
            window.location.href = element[1];
        }
    });
});
